import skvideo
skvideo.setFFmpegPath("D:\TA\rppgenv\Lib\site-packages\skvideo")
from matplotlib import pyplot as plt
from pyVHR.signals.video import Video
from pyVHR.methods.pos import POS
from pyVHR.analysis.testsuite import TestSuite, TestResult
import numpy as np
import cv2
import os


# -- Video object
# videoFilename = "D:\\TA\\dataset\\fake\\aktnlyqpah.mp4"
subdir = "D:/TA/ujicoba/dataset"
for filename  in os.listdir(subdir):
    try:
        videoFilename = os.path.join(subdir, filename)
        video = Video(videoFilename)
        # -- extract faces
        video.getCroppedFaces(detector='mtcnn', extractor='skvideo')
        video.setMask(typeROI='skin_adapt',skinThresh_adapt=0.2)
        params = {"video": video, "verb":0, "ROImask":"skin_adapt", "skinAdapt":0.2}

        # -- invoke the method
        # m = CHROM(**params)
        m = POS(**params)

        # -- invoke the method
        bpmES, timesES = m.runOffline(**params)
        print (bpmES)
        np.save(os.path.join(subdir,filename[:-4]+".npy"), bpmES)
        # video.printVideoInfo()
    except:
        pass
# cv2.imshow('hasil', video.faces[0])
# cv2.waitKey(0)  
  
# #closing all open windows  
# cv2.destroyAllWindows() 