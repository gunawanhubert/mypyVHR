from matplotlib import pyplot as plt
from ..signals.video import Video
from ..methods.pos import POS
from ..analysis.testsuite import TestSuite, TestResult
import numpy as np
import cv2
import os

class Extractor():
    subdir = "D:/TA/ujicoba/dataset"

    def __init__(self,subdir):
        self.subdir = subdir

    def getVHR(self):
        for filename  in os.listdir(self.subdir):
            try:
                videoFilename = os.path.join(self.subdir, filename)
                video = Video(videoFilename)
                # -- extract faces
                video.getCroppedFaces(detector='mtcnn', extractor='skvideo')
                video.setMask(typeROI='skin_adapt',skinThresh_adapt=0.2)
                params = {"video": video, "verb":0, "ROImask":"skin_adapt", "skinAdapt":0.2}

                # -- invoke the method
                # m = CHROM(**params)
                m = POS(**params)

                # -- invoke the method
                bpmES, timesES = m.runOffline(**params)
                np.save(os.path.join(self.subdir,filename[:-4]+".npy"), bpmES)
            except:
                pass
